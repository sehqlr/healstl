'use strict';

$("form").submit(function(e) {
  e.preventDefault();

  var $this = $(this);
  var $submitButton = $this.find('input[type="submit"]');

  $submitButton.attr('value', 'Submitting...').prop('disabled', true);

  var $inputs = $this.find('input:not([type="submit"])'),
    $textareas = $this.find('textarea'),
    $selects = $this.find('select'),
    options = {};

  $.each($inputs, function(i, input) {
    var key   = $(input).attr('name'),
        value = $(input).val();

    options[key] = value;
  });

  $.each($textareas, function(i, textarea) {
    var key   = $(textarea).attr('name'),
        value = $(textarea).val();

    options[key] = value;
  });

  $.each($selects, function(i, select) {
    var key   = $(select).attr('name'),
        value = $(select).find('option:selected').val();

    options[key] = value;
  });

  var post_data = { fields: JSON.stringify(options) },
    post_url = '/admin/submit/applications';

  $.post(post_url, post_data).success(function(data) {
    setTimeout(function() {
      $submitButton.attr('value','Submitted!');
      setTimeout(function() {
        $this.find('input').val('');
        $this.find('textarea').val('');
        $submitButton.attr('value', 'Submit').prop('disabled', false);
      }, 2000);
    }, 1000);
  });
});