<?php

date_default_timezone_set('America/Chicago');

$MYSQL = array(
  'host'  => "us-cdbr-azure-west-a.cloudapp.net",
  'db'    => "as_d0796142dff7e55",
  'user'  => "b7ddd6e91f4dfa",
  'pass'  => "6d5a61ae",
);

try {
	$dbh = new PDO('mysql:host=' . $MYSQL["host"] . ';dbname=' . $MYSQL["db"], $MYSQL["user"], $MYSQL["pass"]);
    $dbh = null;
} catch (PDOException $e) {
  require('setup.php');
}

// global functions
function queryDatabase($statement, $sql_values = null) {
  global $MYSQL;

  $statement_type = explode(' ', trim($statement, '"'))[0];
  $dbh = new PDO('mysql:host=' . $MYSQL["host"] . ';dbname=' . $MYSQL["db"], $MYSQL["user"], $MYSQL["pass"]);
  $sth = $dbh->prepare($statement);
  $sth->execute($sql_values);
  $result = $statement_type == "SELECT" ? $sth->fetchAll(PDO::FETCH_ASSOC) : $dbh->lastInsertId();
  $dbh = null;

  return $result;
}

function getClientIP() {
  $ipaddress = '';
  if (getenv('HTTP_CLIENT_IP'))
      $ipaddress = getenv('HTTP_CLIENT_IP');
  else if(getenv('HTTP_X_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
  else if(getenv('HTTP_X_FORWARDED'))
      $ipaddress = getenv('HTTP_X_FORWARDED');
  else if(getenv('HTTP_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_FORWARDED_FOR');
  else if(getenv('HTTP_FORWARDED'))
     $ipaddress = getenv('HTTP_FORWARDED');
  else if(getenv('REMOTE_ADDR'))
      $ipaddress = getenv('REMOTE_ADDR');
  else
      $ipaddress = 'UNKNOWN';
  return $ipaddress;
}

function tryLogin($email, $password) {
  $hash = queryDatabase("SELECT * FROM volunteers WHERE email = ?", array($email))[0]['password'];
  $isSuccessful = password_verify($password, $hash);

  if ($isSuccessful) { $_SESSION['email'] = $email; }

  return $isSuccessful;
}

if ($url[0] == 'admin') { include($slug . '.php'); }

?>
