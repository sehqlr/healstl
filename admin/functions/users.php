<?php

function getAllVolunteers() {
  return queryDatabase("SELECT * FROM volunteers ORDER BY id DESC");
}

function createVolunteer($first_name, $last_name, $address, $phone, $time) {
  // validate phone uniqueness
  $phones = queryDatabase("SELECT * FROM volunteers WHERE phone = ?", array($phone));

  if (count($phones)) { 
    createResponse("error", "This phone number is already been entered");
  }

  return queryDatabase("INSERT INTO volunteers (first_name, last_name, address, phone, time) VALUES (?, ?, ?, ?, ?)", array($first_name, $last_name, $address, $phone, $time));
}

function updateVolunteer($id, $first_name, $last_name, $address, $phone, $time) {
    return queryDatabase("UPDATE volunteers SET first_name = ?, last_name = ?, address = ?, phone = ?, time = ? WHERE id = ?", array(,$first_name, $last_name, $address, $phone, $time, $id));
}

function deleteVolunteer($id) {
  if (volunteerCount() == 1) { 
    createResponse("error", "Last volunteer cannot be deleted.");
  }

  return queryDatabase("DELETE FROM volunteers WHERE id = ?", array($id));
}

function volunteerCount() {
  return count(queryDatabase("SELECT * FROM volunteers"));
}

?>
