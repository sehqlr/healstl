<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>CodeCause | Users</title>
  
  <link rel="stylesheet" href="assets/css/foundation.min.css" />
  <link rel="stylesheet" href="assets/css/normalize.css" />
  <link rel="stylesheet" href="assets/css/custom.css" />
</head>

<body>    
  <?php if ($slug !== 'login') {?>
  <div class="row">
    <header class="large-8 large-offset-2 columns">
  
    <div id="login">
      <div class="row">
        <div class="large-6 columns">
          <h2>Admin</h2>
        </div>

        <div class="large-4 columns">
          <p class="login-right"><?php echo $_SESSION['email']; ?></p>
        </div>

        <div class="large-2 columns">
          <p><a href="logout" class="login-right">Logout</a></p>
        </div>
    </div>
    <hr>

    <div class="icon-bar three-up">
      <a href="content" class="item<?php echo $slug == "content" ? " active" : ""; ?>">
        <label>Content</label>
      </a>
      <a href="applications" class="item<?php echo $slug == "applications" ? " active" : ""; ?>">
        <label>Applications</label>
      </a>
      <a href="users" class="item<?php echo $slug == "users" ? " active" : ""; ?>">
        <label>Users</label>
      </a>
    </div>

    </header>
  </div>

  <br>
  <?php } ?>

  <div class="row">
    <div class="large-8 large-offset-2 columns">

      <?php file_exists ($file) ? include($file) : include("pages/404.php"); ?>

    </div>
  </div>

  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/app.js"></script>
  <script><?php echo $slug; ?>Functions();</script>
</body>
</html>